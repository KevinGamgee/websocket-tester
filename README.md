# Websocket Tester
## This is for creating a dummy router and/or dummy app for testing the backend or whatever else you want to use it for.


## Install
You need node and preferably yarn but npm will also work

In the project directory:
### `npm install`
or
### `yarn install`

## Available Scripts

In the project directory, you can run:

### `npm start`
or
### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Use
When you have it working in a browser available at the above address it is working.

Modify appMessages.json or routerMessages.json to configure what rows will show up in the browser along with a default message. Each row needs to have a 'type' value in order for the label to show up in each row. Other fields are optional. The button beside the field sends the message by websocket. The default can be modified in the page.

The address of the router and app client are defined in .env. After modifying these you need to restart the router for these values to be loaded.

