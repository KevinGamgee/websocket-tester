

export default class wsHandler {
  CookiesProvider
    setupSocket(stateChange, address) {
        console.log('address', address)
        console.log('stateChange', stateChange)
        
      this.socket = new WebSocket(address)

      this.socket.addEventListener("message", (event) => {
          const {data} = event
          console.log(`Received on ${address}`, data)
          const now = new Date()
          stateChange( `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}.${now.getUTCMilliseconds()} ${JSON.stringify(JSON.parse(data))}`)
      })

      this.socket.addEventListener("close", () => {
        this.setupSocket(stateChange, address)
      })
    }

    submit(event, message) {
        event.preventDefault()
        console.log('sent:', message)
        this.socket.send(
            JSON.stringify(message)
        )
    }
  }