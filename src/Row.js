import React, {Component} from 'react'

import Button from 'react-bootstrap/Button';

export default class Row extends Component{
    constructor(props) {
        super(props);
        this.input = React.createRef();
        this.handleChange = this.handleChange.bind(this);
      }

    send = (event) =>{
        const { message } = this.state
        const { ws } = this.props
        ws.submit(event, message)
    }

    handleChange(event) {
        // this.input.value = event.target.value
        console.log('STATE', event.target.value)
        this.setState({message: event.target.value})
    }

    componentWillMount(){
        const {message} = this.props
        this.setState({message})
    }

    render(){
        const {message} = this.state
        const {type} = this.props.message
        return(
            <div className="row" >
                <div className="col-md-4" >
                    <Button id="button" className="btn btn-primary btn-block" type="submit" style={{borderBottom:2}}
                        onClick={this.send}>{type}
                    </Button>
                </div>
                <div className="col-md-8">
                <input type="text" ref={this.input} className="form-control" defaultValue={JSON.stringify(message)}
                    onChange={this.handleChange}
                />
                </div>
    
            </div>
        )
    }
}