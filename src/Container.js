import React, {Component} from 'react'
import ScrollArea from 'react-scrollbar'
import Cookies from 'js-cookie';

import Row from './Row'
import wsHandler from './wsHandler'


const appMessages = require('./appMessages.json')
const routerMessages = require('./routerMessages.json')

// The addresses of the router and the app client as loaded from .env 
const { REACT_APP_WEB_SOCKET_TEST_ROUTER, REACT_APP_WEB_SOCKET_TEST_CLIENT } = process.env

export default class extends Component{


    componentWillMount(){
        Cookies.remove('device_id')
        Cookies.remove('router_id')
        Cookies.set('device_id', "1235")
        Cookies.set('router_id', "5321")

        const fromBackendToClient=[]
        const fromBackendToRouter=[]
        const fromBackendToClientPost = []

        this.setState({fromBackendToClient, fromBackendToRouter, fromBackendToClientPost})

        this.wsClient = new wsHandler()
        this.wsClient.setupSocket(this.clientReceive, REACT_APP_WEB_SOCKET_TEST_CLIENT)

        this.wsRouter = new wsHandler()
        this.wsRouter.setupSocket(this.routerReceive, REACT_APP_WEB_SOCKET_TEST_ROUTER)
    }

    clientReceive = (backendMessage) =>{
        const {fromBackendToClient} = this.state
        this.setState({fromBackendToClient: [backendMessage, ...fromBackendToClient]})
    }

    routerReceive = (backendMessage) =>{
        const {fromBackendToRouter} = this.state
        this.setState({fromBackendToRouter: [backendMessage, ...fromBackendToRouter]})
    }

    render(){
        const appRows = appMessages.map((message, i) => <Row message={message} ws={this.wsClient} key={i}/>)
        const routerRows = routerMessages.map((message, i) => <Row message={message} ws={this.wsRouter} key={i}/>)

        const{fromBackendToClient, fromBackendToRouter} = this.state

        const clientItems = fromBackendToClient.map((beMessage, i) =>
            <li style={{borderBottom: 20}} key={i} >{beMessage}</li>
        );

        const routerItems = fromBackendToRouter.map((beMessage, i) =>
            <li style={{borderBottom: 20}} key={i} >{beMessage}</li>
        );

        return(
            <div>
                <h1 className="h3 mb-3 font-weight-normal">Websocket Test</h1>
                <h2 className="h3 mb-3 font-weight-normal">Put JSON here.</h2>
                <h3 className="h3 mb-3 font-weight-normal">Every object needs a type and a body {JSON.stringify({"type":"whatever", "body":{"other":"stuff"}})}</h3>
                    
                <div className="row">
                    <div className="col-md-6">
                        <h3 className="h3 mb-3 font-weight-normal">Send a message from the router to the backend</h3>
                        {routerRows}
                        <h3 className="h3 mb-3 font-weight-normal">Router messages from backend</h3>
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName="content"
                            horizontal={false}
                            style={{height:300}}
                            >
                            <div><ul style={{listStyle:'none', textAlign:'left'}}>{routerItems}</ul></div>
                        </ScrollArea>
                    </div>
                    <div className="col-md-6">
                        <h3 className="h3 mb-3 font-weight-normal">Send a websocket message from the app to the backend</h3>
                        {appRows}
                        <h3 className="h3 mb-3 font-weight-normal">App messages from backend</h3>
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName="content"
                            horizontal={false}
                            style={{height:300}}
                            >
                            <div><ul style={{listStyle:'none', textAlign:'left'}}>{clientItems}</ul></div>
                        </ScrollArea>
                    </div>
                </div>
            </div>
        )   
    }
}